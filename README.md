# Linegate Universal Minifier

Light universal minifier for html+css, php and js written in c++ in translation style with renaming variables/classes feature

Progress:
* [x]  HTML basic minification
* [ ]  HTML+CSS minification
* [ ]  JS minification
* [ ]  PHP minification
* [ ]  Rename feature